#include <iostream>
#include <vector>
#include <cmath>
#ifndef NEURAL_NETWORK_NEURAL_NETWORK_H
#define NEURAL_NETWORK_NEURAL_NETWORK_H

#endif //NEURAL_NETWORK_NEURAL_NETWORK_H

class NeuralNetwork{
private:
    std::vector<int> layers_size;
public:
    NeuralNetwork(std::vector<int>& layers): layers_size(layers) {}
};

class ActivationFunction{
private:
    std::vector<double> input;
    std::vector<double> output;
public:
    ActivationFunction(const std::vector<double>& vec): input(vec) {}

    std::vector<double> apply_function(){
        output.clear();
        for (double value: input){
            output.push_back(atan(value));
        };
        return output;
    };

    std::vector<double> apply_derivative(){
        for (double value: input){
            output.push_back(1/(1 + value * value));
        };
        return output;
    };
};

class LossFunction{
private:
    std::vector<double> prediction;
    std::vector<double> real_result;
public:
    LossFunction(const std::vector<double>& pred, std::vector<double>& result): prediction(pred), real_result(result){};
    double distance() const{
        double distance = 0.0;
        for (size_t i = 0; i < prediction.size(); ++i) {
            distance += pow(prediction[i] - real_result[i], 2);
        };
        return sqrt(distance);
    };
};